CONTENTS OF THIS FILE:-
-----------------------

  * Overview
  * Features
  * Requirements
  * Future enhancements


Overview:-
----------

The Spam Filter module checkes the text content of nodes and comments for 
existence of any spam-text as a substring. If any spam-text is found the 
corresponding node or comment is set as unpublished. The spam-text are stored 
on database and new text can be added, deleted and edited using the UI 
provided by the module. There is a Validate Content section using which a 
text-paragraph can be tested against the current list of spam text.


Features:-
----------
* Users can add/delete/edit spam-text which makes the spam-filter dynamic.
* "Validate Content" section can be used to test a paragraph of text for spam.


Requirements:-
--------------
This module is for drupal 7.
